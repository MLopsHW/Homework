import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def clean_data(input_path: str, output_path: str):
    """Function removes excess columns and enforces
    correct data types.
    :param input_path: Path to read filtered by region DataFrame
    :param output_path: Path to save cleaned DataFrame
    :return:
    """

    df = pd.read_csv(input_path)
    df.drop(["compl", "version_info"], inplace=True)
    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    clean_data()
