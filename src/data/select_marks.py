import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def select_marks(input_path: str, output_path: str):
    """Function selects the listings belonging to a specified region.
    :param input_path: Path to read clean DataFrame
    :param output_path: Path to save filtered by region DataFrame
    """
    df = pd.read_csv(input_path)
    marks = df['car'].str.split(' ', expand=True)[0] + ' ' + df['car'].str.split(' ', expand=True)[1].unique()
    print(f"Selected {len(df)} marks of LADA")
    marks.to_csv(output_path, index=False)


if __name__ == "__main__":
    select_marks()
