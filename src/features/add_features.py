import click
import pandas as pd

damage_map = {'damaged door': ["Дверь передняя правая", "Дверь передняя левая",
                               "Дверь задняя правая", "Дверь задняя левая"],
              'damaged window': ["Стекло переднее правое", "Стекло переднее левое", "Стекло заднее правое",
                                 "Стекло заднее левое"],
              'damaged headlight': ["Фара правая", "Фара левая"],
              'damaged mirror': ["Левое зеркало", "Правое зеркало"],
              'damaged hood': ["Капот"],
              'damaged bumper': ["Бампер"],
              'damaged wind shield': ["Стекло ветровое"]}


def get_catalog_num(model, df):
    result = {}

    for k, v in damage_map.items():
        result[k] = {}
        for j in v:
            search = generate_reg(j)
            result.values()
            try:
                res = df[(df['car'].str.lower().str.contains(model.lower())) & (
                    df['classification'].str.lower().str.contains('кузов')) & (
                             df['name'].str.lower().str.contains(search))].iloc[0].to_dict()["catalog_number"]
            except:
                res = None
            result[k][j] = res

    return result


def generate_reg(list_str):
    res = []
    for i in list_str.lower().split(" "):
        if len(i) > 5:
            res.append(i[:len(i) - 2] + ".{2}")
        elif len(i) > 3:
            res.append(i[:len(i) - 1] + ".{1}")
        else:
            res.append(i)
    res = ".*".join(res)
    return res


@click.command()
@click.argument('input_path_marks', type=click.Path(exists=True))
@click.argument('input_path_data', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def add_features(input_path_marks: str, input_path_data: str, output_path: str):
    """Function removes excess columns and enforces
    correct data types.
    :param input_path_marks: Path to read filtered by region DataFrame
    :param input_path_data: Path to read filtered by region DataFrame
    :param output_path: Path to save DataFrame with features
    :return:
    """
    model_list = pd.read_csv(input_path_marks, index_col=False).tolist()
    df = pd.read_csv(input_path_data)

    result_of_num = {}

    for model in model_list:
        model = model.lower()
        result_of_num[model] = get_catalog_num(model, df)

    with open(output_path, "w") as file:
        file.write(str(result_of_num))


if __name__ == "__main__":
    add_features()
