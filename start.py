import src

RAW_DATA_PATH = "./data/raw/normalized.csv"
DATA_MARKS_PATH = "./data/interim/data_marks.csv"
CLEANED_DATA_PATH = "./data/interim/data_cleaned.csv"
FEATURED_DATA_PATH = "./data/processed/data_featured.txt"

if __name__ == "__main__":
    src.select_marks(RAW_DATA_PATH, DATA_MARKS_PATH)
    src.clean_data(DATA_MARKS_PATH, CLEANED_DATA_PATH)
    src.add_features(DATA_MARKS_PATH, CLEANED_DATA_PATH, FEATURED_DATA_PATH)