from typing import Any

import requests
from flask import Flask, request, render_template, jsonify

app = Flask(__name__)
text_list: list[Any] = []


@app.route("/hello")  # Обработчик для корневой страницы
def hello():
    name = request.args.get("name")  # Получаем значение query-параметра 'name'
    if name:
        return f"Привет, {name}!"
    else:
        return "Добро пожаловать на страницу!"


@app.route("/")
def index():
    return render_template("index.html", text_list=text_list)


# Замените 'YOUR_API_KEY' на ваш API ключ от OpenWeatherMap
API_KEY = "99c9654c1f4407eab6859ca081ee3d94"


# Обработчик для получения погоды в Екатеринбурге
@app.route("/weather")
def get_weather():
    # Определите координаты для Екатеринбурга (или любого другого города)
    city = "Yekaterinburg,ru"
    url = (
        f"http://api.openweathermap.org/data/2.5/weather?q={city}&"
        f"appid={API_KEY}&units=metric"
    )
    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()
        # Извлеките информацию о погоде из JSON-ответа
        temperature: object = data["main"]["temp"]
        description: object = data["weather"][0]["description"]
        return jsonify(dict(temperature=temperature, description=description))
    else:
        return "Не удалось получить информацию о погоде"


if __name__ == "__main__":
    app.run(debug=True)
